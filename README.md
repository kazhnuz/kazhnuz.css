# kazhnuz.css

Le depot contenant ma charte graphique, utilisée par mon blog kazhnuz.space et plusieurs de mes autres sites personnels (contrairement à la charte kobold.city qui est plutôt utilisée par mes sites de projet partagés, comme rulebook et mes univers).

Cette charte graphique est basé sur la palette de couleur [Solarized](https://ethanschoonover.com/solarized/), et à pour objectif d'avoir une esthétique un peu "vieille affiche" et flat. Quelques fichier html contenant des mockups de mon blog et de son ancien projet de section admin restent présent.
