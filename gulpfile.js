const { src, dest, parallel } = require('gulp');
const include = require('gulp-include');
const sass = require('gulp-sass');

sass.compiler = require('node-sass');

function html() {
  return src('src/static/*.html')
    .pipe(include())
    .pipe(dest('tmp'))
}

function css() {
  return src('src/scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('tmp'))
}

function js() {
  return src('src/js/*.js')
    .pipe(dest('tmp/js'))
}

function htmladmin() {
  return src('src/admin/*.html')
    .pipe(include())
    .pipe(dest('tmp/admin'))
}

function cssadmin() {
  return src('src/admin/scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('tmp/admin'))
}

function jsadmin() {
  return src('src/admin/js/*.js')
    .pipe(dest('tmp/admin/js'))
}

function dep() {
  return src(['dep/**/*'])
    .pipe(dest('tmp/dep'));
}

function assets() {
  return src(['assets/**/*'])
    .pipe(dest('tmp'));
}

exports.html = html;
exports.css  = css;
exports.js  = js;
exports.dep  = dep;
exports.assets  = assets;
exports.default = parallel(html, css, js, dep, assets, htmladmin, cssadmin, jsadmin);
