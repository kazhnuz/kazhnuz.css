# CHANGELOG

Tout les changements notables au projet sont consignés dans ce fichier, afin de simplement voir les évolutions.

Ce format est basé sur la norme [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et ce projet adhère au [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## En cours

Version initiale basée sur la structure actuelle de kazhnuz.space
