var length = 35;

var texts = document.getElementsByClassName('trim-that');

for (var i = 0; i < texts.length; i++) {
    var text = texts[i];
    var string = text.innerHTML
    var trimmedString = string.length > length ?
        string.substring(0, length - 3) + "..." :
        string

    text.innerHTML = trimmedString
	console.log(text.innerHTML);
}